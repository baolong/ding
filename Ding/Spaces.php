<?php

namespace Ding;

use Ding\Contracts\BasicDing;
use Ding\Contracts\Tools;
use think\admin\extend\HttpExtend;

/**
 * 钉钉钉盘空间管理
 * Class User
 * @package Ding
 */
class Spaces extends BasicDing
{


    /**
     * 获取角色列表
     * @param int $offset
     * @param int $size
     * @return array
     * @throws Exceptions\InvalidResponseException
     * @throws \WeChat\Exceptions\InvalidResponseException
     * @throws \WeChat\Exceptions\LocalCacheException
     */
    public function list($unionId,$parentId,$nextToken,$maxResults){
        $url = "https://api.dingtalk.com/v1.0/drive/spaces/706290188/files";
        if (empty($this->access_token))  $this->access_token = $this->getAccessToken();

        $options['headers'][] = "x-acs-dingtalk-access-token: {$this->access_token}";
        return Tools::json2arr(HttpExtend::get( $url,
            ['unionId' => $unionId,'parentId' => $parentId,'nextToken'=>$nextToken,'maxResults'=>$maxResults,'withIcon' =>1],
            $options
        ));
    }

    /**
     * 获取文件上传信息
     * @param $spaceId
     * @param $unionId
     * @return array
     * @throws Exceptions\InvalidResponseException
     * @throws \WeChat\Exceptions\InvalidResponseException
     * @throws \WeChat\Exceptions\LocalCacheException
     */
    public function getUploadInformation($parentDentryUuid,$unionId){
//        $url = "https://api.dingtalk.com/v2.0/storage/spaces/files/{$parentDentryUuid}/uploadInfos/query?unionId={$unionId}";
        $url = "https://api.dingtalk.com/v1.0/storage/spaces/{$parentDentryUuid}/files/uploadInfos/query?unionId={$unionId}";
        if (empty($this->access_token))  $this->access_token = $this->getAccessToken();
        $options['headers'][] = "x-acs-dingtalk-access-token: {$this->access_token}";
        return Tools::json2arr(HttpExtend::post( $url, ['protocol' => 'HEADER_SIGNATURE','multipart' => 'false'], $options ));
    }

    /**
     * 提交附件
     * @param $uploadKey
     * @param $name
     * @return array
     * @throws Exceptions\InvalidResponseException
     * @throws \WeChat\Exceptions\InvalidResponseException
     * @throws \WeChat\Exceptions\LocalCacheException
     */
    public function commit($parentDentryUuid,$unionId,$uploadKey,$name) : array{
//        $url = "https://api.dingtalk.com/v2.0/storage/spaces/files/{$parentDentryUuid}/commit?unionId={$unionId}";
        $url = "https://api.dingtalk.com//v1.0/storage/spaces/{$parentDentryUuid}/files/commit?unionId={$unionId}";
        if (empty($this->access_token))  $this->access_token = $this->getAccessToken();
        $options['headers'][] = "x-acs-dingtalk-access-token: {$this->access_token}";
        $options['headers'][] = "Content-Type: application/json";

        $data = [
            'uploadKey' => $uploadKey,
            'name' => $name,
            'parentId ' => "0",
            'option' => [
                'size' => '1024',
                'conflictStrategy' => 'OVERWRITE',
                'convertToOnlineDoc' => false,
            ],
        ];
        $data = json_encode($data,JSON_UNESCAPED_UNICODE);


        return Tools::json2arr(HttpExtend::post( $url, $data , $options ));
    }

}