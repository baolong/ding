<?php

namespace Ding;

use Ding\Contracts\BasicDing;
use Ding\Contracts\Tools;
use think\admin\extend\HttpExtend;

/**
 * 钉钉用户管理
 * Class User
 * @package Ding
 */
class User extends BasicDing
{

    /**
     * 创建用户
     * @param array $data
     * @return array
     * @throws Exceptions\InvalidResponseException
     * @throws \WeChat\Exceptions\InvalidResponseException
     * @throws \WeChat\Exceptions\LocalCacheException
     */
    public function create(array $data){
        $url = "https://oapi.dingtalk.com/topapi/v2/user/create?access_token=ACCESS_TOKEN";
        $this->registerApi($url, __FUNCTION__, func_get_args());
        return Tools::json2arr(HttpExtend::post($url,$data));
    }

    public function getbyunionid($unionid){
        $url = "https://oapi.dingtalk.com/topapi/user/getbyunionid?access_token=ACCESS_TOKEN";
        $this->registerApi($url, __FUNCTION__, func_get_args());
        return Tools::json2arr(HttpExtend::post($url,['unionid' => $unionid]));
    }

    /**
     * 更新用户
     * @return array
     * @throws Exceptions\InvalidResponseException
     * @throws \WeChat\Exceptions\InvalidResponseException
     * @throws \WeChat\Exceptions\LocalCacheException
     */
    public function update(array $data){
        $url = "https://oapi.dingtalk.com/topapi/v2/user/update?access_token=ACCESS_TOKEN";
        $this->registerApi($url, __FUNCTION__, func_get_args());
        return Tools::json2arr(HttpExtend::post($url,$data));
    }

    /**
     * 删除用户
     * @param string $userid
     * @return array
     * @throws Exceptions\InvalidResponseException
     * @throws \WeChat\Exceptions\InvalidResponseException
     * @throws \WeChat\Exceptions\LocalCacheException
     */
    public function delete(string $userid=''){
        $url = "https://oapi.dingtalk.com/topapi/v2/user/update?access_token=ACCESS_TOKEN";
        $this->registerApi($url, __FUNCTION__, func_get_args());
        return Tools::json2arr(HttpExtend::get($url,['userid' => $userid]));
    }

    /**
     * 获取用户详情
     * @param string $userid
     * @return array
     * @throws Exceptions\InvalidResponseException
     * @throws \WeChat\Exceptions\InvalidResponseException
     * @throws \WeChat\Exceptions\LocalCacheException
     */
    public function get(string $userid=''){
        $url = "https://oapi.dingtalk.com/topapi/v2/user/get?access_token=ACCESS_TOKEN";
        $this->registerApi($url, __FUNCTION__, func_get_args());
        return Tools::json2arr(HttpExtend::get($url,['userid' => $userid,'lang' => 'zh_CN']));
    }

    /**
     * 获取用户userid
     * @param string $code
     * @return array
     * @throws Exceptions\InvalidResponseException
     * @throws \WeChat\Exceptions\InvalidResponseException
     * @throws \WeChat\Exceptions\LocalCacheException
     */
    public function getuserinfo(string $code=''){
        $url = "https://oapi.dingtalk.com/topapi/v2/user/getuserinfo?access_token=ACCESS_TOKEN";
        $this->registerApi($url, __FUNCTION__, func_get_args());
        return Tools::json2arr(HttpExtend::get($url,['code' => $code]));
    }

    /**
     * 获取通讯录会员信息
     * @param $unionId
     * @return array
     * @throws Exceptions\InvalidResponseException
     */
    public function getContactUser($unionId = 'me'){
        $url = "https://api.dingtalk.com/v1.0/contact/users/{$unionId}";

        $options['headers'][] = "x-acs-dingtalk-access-token:{$this->user_access_token}";
        return Tools::json2arr(HttpExtend::get($url,'',$options));
    }

    /**
     * 获取部门用户userid列表
     * @param string $deptId
     * @return array
     * @throws Exceptions\InvalidResponseException
     * @throws \WeChat\Exceptions\InvalidResponseException
     * @throws \WeChat\Exceptions\LocalCacheException
     */
    public function getDeptMember(string $deptId){
        $url = "https://oapi.dingtalk.com/topapi/user/listid?access_token=ACCESS_TOKEN";
        $this->registerApi($url, __FUNCTION__, func_get_args());
        return Tools::json2arr(HttpExtend::get($url,['dept_id' => $deptId]));
    }

    /**
     * 获取部门用户
     * @param string $deptId
     * @return array
     * @throws Exceptions\InvalidResponseException
     * @throws \WeChat\Exceptions\InvalidResponseException
     * @throws \WeChat\Exceptions\LocalCacheException
     */
    public function simplelist(string $deptId){
        $url = "https://oapi.dingtalk.com/topapi/v2/user/simplelist?access_token=ACCESS_TOKEN";
        $this->registerApi($url, __FUNCTION__, func_get_args());
        return Tools::json2arr(HttpExtend::get($url,['department_id' => $deptId]));
    }

    /**
     * 获取部门用户详情
     * @param string $deptId
     * @return array
     * @throws Exceptions\InvalidResponseException
     * @throws \WeChat\Exceptions\InvalidResponseException
     * @throws \WeChat\Exceptions\LocalCacheException
     */
    public function listbypage(string $deptId='',$offset=0,$size=50,$order='entry_asc'){
        $url = "https://oapi.dingtalk.com/topapi/v2/user/listbypage?access_token=ACCESS_TOKEN";
        $this->registerApi($url, __FUNCTION__, func_get_args());
        $data = ['department_id' => $deptId,'offset'=>$offset,'size' => $size,'order'=>$order];
        return Tools::json2arr(HttpExtend::get($url,$data));
    }



    /**
     * 获取管理员列表
     * @return array
     * @throws Exceptions\InvalidResponseException
     * @throws \WeChat\Exceptions\InvalidResponseException
     * @throws \WeChat\Exceptions\LocalCacheException
     */
    public function getAdmin(){
        $url = "https://oapi.dingtalk.com/topapi/v2/user/get_admin?access_token=ACCESS_TOKEN";
        $this->registerApi($url, __FUNCTION__, func_get_args());
        return Tools::json2arr(HttpExtend::get($url));
    }

    /**
     * 获取管理员通讯录权限范围
     * @param string $userid 员工id
     * @return array
     * @throws Exceptions\InvalidResponseException
     * @throws \WeChat\Exceptions\InvalidResponseException
     * @throws \WeChat\Exceptions\LocalCacheException
     */
    public function getAdminScope(string $userid){
        $url = "https://oapi.dingtalk.com/topapi/user/get_admin_scope?access_token=ACCESS_TOKEN";
        $this->registerApi($url, __FUNCTION__, func_get_args());
        return Tools::json2arr(HttpExtend::get($url,['userid' => $userid]));
    }

    /**
     * 根据unionid获取userid
     * @param string $unionid 员工在当前开发者企业账号范围内的唯一标识，系统生成，固定值，不会改变
     * @return array
     * @throws Exceptions\InvalidResponseException
     * @throws \WeChat\Exceptions\InvalidResponseException
     * @throws \WeChat\Exceptions\LocalCacheException
     */
    public function getUseridByUnionid(string $unionid){
        $url = "https://oapi.dingtalk.com/topapi/v2/user/getUseridByUnionid?access_token=ACCESS_TOKEN";
        $this->registerApi($url, __FUNCTION__, func_get_args());
        return Tools::json2arr(HttpExtend::get($url,['unionid' => $unionid]));
    }

    /**
     * 根据手机号获取userid
     * @param string $mobile 手机号码
     * @return array
     * @throws Exceptions\InvalidResponseException
     * @throws \WeChat\Exceptions\InvalidResponseException
     * @throws \WeChat\Exceptions\LocalCacheException
     */
    public function getByMobile(string $mobile){
        $url = "https://oapi.dingtalk.com/topapi/v2/user/get_by_mobile?access_token=ACCESS_TOKEN";
        $this->registerApi($url, __FUNCTION__, func_get_args());
        return Tools::json2arr(HttpExtend::get($url,['mobile' => $mobile]));
    }

    /**
     * 获取企业员工人数
     * @param int $onlyActive 0：包含未激活钉钉的人员数量 1：不包含未激活钉钉的人员数量
     * @return array
     * @throws Exceptions\InvalidResponseException
     * @throws \WeChat\Exceptions\InvalidResponseException
     * @throws \WeChat\Exceptions\LocalCacheException
     */
    public function getOrgUserCount(int $onlyActive=0){
        $url = "https://oapi.dingtalk.com/topapi/v2/user/get_org_user_count?access_token=ACCESS_TOKEN";
        $this->registerApi($url, __FUNCTION__, func_get_args());
        return Tools::json2arr(HttpExtend::get($url,['onlyActive' => $onlyActive]));
    }

}