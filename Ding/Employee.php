<?php

namespace Ding;

use Ding\Contracts\BasicDing;
use Ding\Contracts\Tools;
use think\admin\extend\HttpExtend;

/**
 * 钉钉用户 管理
 * Class User
 * @package Ding
 */
class Employee extends BasicDing
{

    /**
     * 创建用户
     * @param array $data
     * @return array
     * @throws Exceptions\InvalidResponseException
     * @throws \WeChat\Exceptions\InvalidResponseException
     * @throws \WeChat\Exceptions\LocalCacheException
     */
    public function querypreentry(array $data){
        $url = "https://oapi.dingtalk.com/topapi/smartwork/hrm/employee/querypreentry?access_token=ACCESS_TOKEN";
        $this->registerApi($url, __FUNCTION__, func_get_args());
        return Tools::json2arr(HttpExtend::post($url,$data));
    }

    /**
     * 智能人事获取员工花名册信息
     * @param array $data
     * @return array
     * @throws Exceptions\InvalidResponseException
     * @throws \WeChat\Exceptions\InvalidResponseException
     * @throws \WeChat\Exceptions\LocalCacheException
     */
    public function list(array $data){
        $data ['agentid'] = $this->config->get('agent_id');
        $url = "https://oapi.dingtalk.com/topapi/smartwork/hrm/employee/v2/list?access_token=ACCESS_TOKEN";
        $this->registerApi($url, __FUNCTION__, func_get_args());
        return Tools::json2arr(HttpExtend::post($url,$data ));
    }

    /**
     * 智能人事更新员工档案信息
     * @return array
     * @throws Exceptions\InvalidResponseException
     * @throws \WeChat\Exceptions\InvalidResponseException
     * @throws \WeChat\Exceptions\LocalCacheException
     */
    public function update(){
        $url = "https://oapi.dingtalk.com/topapi/smartwork/hrm/employee/v2/update?access_token=ACCESS_TOKEN";
        $this->registerApi($url, __FUNCTION__, func_get_args());
        return Tools::json2arr(HttpExtend::post($url,$data));
    }

    /**
     * 获取花名册元数据
     * @return array
     * @throws Exceptions\InvalidResponseException
     * @throws \WeChat\Exceptions\InvalidResponseException
     * @throws \WeChat\Exceptions\LocalCacheException
     */
    public function get(){
        $data ['agentid'] = $this->config->get('agent_id');
        $url = "https://oapi.dingtalk.com/topapi/smartwork/hrm/roster/meta/get?access_token=ACCESS_TOKEN";
        $this->registerApi($url, __FUNCTION__, func_get_args());
        return Tools::json2arr(HttpExtend::post($url,$data ));
    }

}