<?php

namespace Ding;

use Ding\Contracts\BasicDing;
use Ding\Contracts\Tools;
use think\admin\extend\HttpExtend;

/**
 * 钉钉角色管理
 * Class User
 * @package Ding
 */
class Role extends BasicDing
{


    /**
     * 获取角色列表
     * @param int $offset
     * @param int $size
     * @return array
     * @throws Exceptions\InvalidResponseException
     * @throws \WeChat\Exceptions\InvalidResponseException
     * @throws \WeChat\Exceptions\LocalCacheException
     */
    public function list(int $size=100,int $offset=0){
        $url = "https://oapi.dingtalk.com/topapi/role/list?access_token=ACCESS_TOKEN";
        $this->registerApi($url, __FUNCTION__, func_get_args());
        return Tools::json2arr(HttpExtend::get($url,['size' => $size,'offset' => $offset]));
    }

    /**
     * 获取指定角色的员工列表
     * @param string $role_id
     * @param int $size
     * @param int $offset
     * @return array
     * @throws Exceptions\InvalidResponseException
     * @throws \WeChat\Exceptions\InvalidResponseException
     * @throws \WeChat\Exceptions\LocalCacheException
     */
    public function simplelist(string $role_id,int $size=100,int $offset=0){
        $url = "https://oapi.dingtalk.com/topapi/role/simplelist?access_token=ACCESS_TOKEN";
        $this->registerApi($url, __FUNCTION__, func_get_args());
        return Tools::json2arr(HttpExtend::post($url,['role_id' => $role_id,'size' => $size,'offset' => $offset]));
    }

    /**
     * 获取角色组列表
     * @param int $size
     * @param int $offset
     * @return array
     * @throws Exceptions\InvalidResponseException
     * @throws \WeChat\Exceptions\InvalidResponseException
     * @throws \WeChat\Exceptions\LocalCacheException
     */
    public function getRoleGroup(int $group_id){
        $url = "https://oapi.dingtalk.com/topapi/role/getrolegroup?access_token=ACCESS_TOKEN";
        $this->registerApi($url, __FUNCTION__, func_get_args());
        return Tools::json2arr(HttpExtend::get($url,['group_id' => $group_id]));
    }

}