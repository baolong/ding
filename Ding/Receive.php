<?php

namespace Ding;

use Ding\Contracts\BasicDing;
use Ding\Contracts\DingtalkCrypt;
use Ding\Contracts\Tools;
use think\admin\extend\HttpExtend;
use think\App;
use think\facade\Log;
use think\Response;
use think\response\Json;

/**
 * 钉钉用户管理
 * Class User
 * @package Ding
 */
class Receive extends BasicDing
{

    public $crypt;
    public $data;
    public function __construct(array $options)
    {
        parent::__construct($options);
        $this->signature = @$_GET["signature"];
        $this->timeStamp = @$_GET["timestamp"];
        $this->nonce     = @$_GET["nonce"];
        $postdata  = file_get_contents("php://input");
        $postList  = json_decode($postdata, true);
        $encrypt   = $postList["encrypt"];
       // $encrypt   = 'oX+HoGdzVjm87aHUgnGq2BMjlXrZ3OPd0LjVi80CjldEqn6hVzw3LUglRIc8Nf1wXv7fcOlzNKZQOUoqN3a5mG2NOUVuMrikYp0nU25YYePmQ3vBxzkxg7CKGMfkQnXA';
        $this->crypt     = new DingtalkCrypt($options['token'],$options['aes_key'],$options['corp_id']);
        // 消息解密
        $msg       = "";
        $errCode   = $this->crypt->DecryptMsg($this->signature, $this->timeStamp, $this->nonce, $encrypt, $msg);
        $this->data  = json_decode($msg, true);


        pin( json_encode($this->data) );

        if($this->data['EventType'] == 'check_url'){
            if ($this->crypt->EncryptMsg('success', $this->timeStamp, $this->nonce, $encryptMsg) == 0) {
                die(json_encode($encryptMsg,JSON_UNESCAPED_UNICODE));
            }
        }
    }


}