<?php

// +----------------------------------------------------------------------
// | WeChatDeveloper
// +----------------------------------------------------------------------
// | 版权所有 2014~2018 广州楚才信息科技有限公司 [ http://www.cuci.cc ]
// +----------------------------------------------------------------------
// | 官方网站: http://think.ctolog.com
// +----------------------------------------------------------------------
// | 开源协议 ( https://mit-license.org )
// +----------------------------------------------------------------------
// | github开源项目：https://github.com/zoujingli/WeChatDeveloper
// +----------------------------------------------------------------------

namespace Ding\Contracts;

use Ding\Contracts\Tools;
use Ding\Exceptions\InvalidArgumentException;
use Ding\Exceptions\InvalidResponseException;
use think\admin\Exception;
use think\exception\HttpResponseException;

const OAPI_HOST = "https://oapi.dingtalk.com";
/**
 * Class BasicWeChat
 * @package WeChat\Contracts
 */
class BasicDing
{

    /**
     * 当前微信配置
     * @var DataArray
     */
    public $config;

    /**
     * 访问AccessToken
     * @var string
     */
    public $access_token = '';

    /**
     * 用户token
     * @var string
     */
    public $user_access_token = '';

    /**
     * 当前请求方法参数
     * @var array
     */
    protected $currentMethod = [];

    /**
     * 当前模式
     * @var bool
     */
    protected $isTry = false;

    /**
     * 静态缓存
     * @var static
     */
    protected static $cache;

    /**
     * 注册代替函数
     * @var string
     */
    protected $GetAccessTokenCallback;

    public $crypt;

    /**
     * BasicWeChat constructor.
     * @param array $options
     */
    public function __construct(array $options)
    {
        if (empty($options['app_key'])) {
            throw new InvalidArgumentException("Missing Config -- [appkey]");
        }
        if (empty($options['app_secret'])) {
            throw new InvalidArgumentException("Missing Config -- [appsecret]");
        }
        if (!empty($options['cache_path'])) {
            Tools::$cache_path = $options['cache_path'];
        }

        $this->config = new DataArray($options);

        if (!empty( $this->config->get('token') ) && !empty( $this->config->get('aes_key') )){
            // http TOKEN , http aes key , 应用  AppKey
            $this->crypt = new DingtalkCrypt( $this->config->get('token'), $this->config->get('aes_key') , $this->config->get('app_key') );
        }
    }

    /**
     * 静态创建对象
     * @param array $config
     * @return static
     */
    public static function instance(array $config)
    {
        $key = md5(get_called_class() . serialize($config));
        if (isset(self::$cache[$key])) return self::$cache[$key];
        return self::$cache[$key] = new static($config);
    }

    /**
     * @return void
     */
    public function getUserAccessToken($authCode){

        $data = [
            'clientId' => $this->config->get('app_key'),
            'clientSecret' => $this->config->get('app_secret'),
            'code' => $authCode,
            'grantType' => 'authorization_code',
        ];

        $url = "https://api.dingtalk.com/v1.0/oauth2/userAccessToken";

        $options['headers'][] = "Content-Type:application/json";
        $data = json_encode($data,JSON_UNESCAPED_UNICODE);
        $result = Tools::json2arr(Tools::post($url,$data,$options));

        if (isset($result['code']) && !empty($result['code'])){
            throw new InvalidArgumentException($result['message']);
        }

        return $this->user_access_token = $result['accessToken']??'';
    }


    /**
     * 获取访问accessToken
     * @return string
     * @throws \WeChat\Exceptions\InvalidResponseException
     * @throws \WeChat\Exceptions\LocalCacheException
     */
    public function getAccessToken()
    {
        if (!empty($this->access_token)) {
            return $this->access_token;
        }

        $cache = $this->config->get('agent_id') . '_access_token';
        $this->access_token = Tools::getCache($cache);
        if (!empty($this->access_token)) {
            return $this->access_token;
        }
        // 处理开放平台授权公众号获取AccessToken
        if (!empty($this->GetAccessTokenCallback) && is_callable($this->GetAccessTokenCallback)) {
            $this->access_token = call_user_func_array($this->GetAccessTokenCallback, [$this->config->get('agent_id'), $this]);
            if (!empty($this->access_token)) {
                Tools::setCache($cache, $this->access_token, 7000);
            }
            return $this->access_token;
        }
        list($appkey, $secret) = [$this->config->get('app_key'), $this->config->get('app_secret')];
        $url = "https://oapi.dingtalk.com/gettoken?appkey={$appkey}&appsecret={$secret}";
        $result = Tools::json2arr(Tools::get($url));
        if (!empty($result['access_token'])) {
            Tools::setCache($cache, $result['access_token'], 7000);
        }
        return $this->access_token = $result['access_token'];
    }

    /**
     * 设置外部接口 AccessToken
     * @param string $access_token
     * @throws \WeChat\Exceptions\LocalCacheException
     * @author 高一平 <iam@gaoyiping.com>
     *
     * 当用户使用自己的缓存驱动时，直接实例化对象后可直接设置 AccessToekn
     * - 多用于分布式项目时保持 AccessToken 统一
     * - 使用此方法后就由用户来保证传入的 AccessToekn 为有效 AccessToekn
     */
    public function setAccessToken($access_token)
    {
        if (!is_string($access_token)) {
            throw new InvalidArgumentException("Invalid AccessToken type, need string.");
        }
        $cache = $this->config->get('agent_id') . '_access_token';
        Tools::setCache($cache, $this->access_token = $access_token);
    }

    /**
     * 清理删除 AccessToken
     * @return bool
     */
    public function delAccessToken()
    {
        $this->access_token = '';
        return Tools::delCache($this->config->get('agent_id') . '_access_token');
    }

    /**
     * 以GET获取接口数据并转为数组
     * @param string $url 接口地址
     * @return array
     * @throws InvalidResponseException
     * @throws \WeChat\Exceptions\LocalCacheException
     */
    protected function httpGetForJson($url)
    {
        try {
            return Tools::json2arr(Tools::get($url));
        } catch (InvalidResponseException $e) {
            if (isset($this->currentMethod['method']) && empty($this->isTry)) {
                if (in_array($e->getCode(), ['40014', '40001', '41001', '42001'])) {
                    $this->delAccessToken();
                    $this->isTry = true;
                    return call_user_func_array([$this, $this->currentMethod['method']], $this->currentMethod['arguments']);
                }
            }
            throw new InvalidResponseException($e->getMessage(), $e->getCode());
        }
    }

    /**
     * 以POST获取接口数据并转为数组
     * @param string $url 接口地址
     * @param array $data 请求数据
     * @param bool $buildToJson
     * @return array
     * @throws InvalidResponseException
     * @throws \WeChat\Exceptions\LocalCacheException
     */
    protected function httpPostForJson($url, array $data, $buildToJson = true)
    {
        try {
            return Tools::json2arr(Tools::post($url, $buildToJson ? Tools::arr2json($data) : $data));
        } catch (InvalidResponseException $e) {
            if (!$this->isTry && in_array($e->getCode(), ['40014', '40001', '41001', '42001'])) {
                [$this->delAccessToken(), $this->isTry = true];
                return call_user_func_array([$this, $this->currentMethod['method']], $this->currentMethod['arguments']);
            }
            throw new InvalidResponseException($e->getMessage(), $e->getCode());
        }
    }

    /**
     * 注册当前请求接口
     * @param string $url 接口地址
     * @param string $method 当前接口方法
     * @param array $arguments 请求参数
     * @return mixed
     * @throws \WeChat\Exceptions\InvalidResponseException
     * @throws \WeChat\Exceptions\LocalCacheException
     */
    protected function registerApi(&$url, $method, $arguments = [])
    {
        $this->currentMethod = ['method' => $method, 'arguments' => $arguments];
        if (empty($this->access_token)) {
            $this->access_token = $this->getAccessToken();
        }
     
        return $url = str_replace('ACCESS_TOKEN', $this->access_token, $url);
    }


    /**
     * 接口通用POST请求方法
     * @param string $url 接口URL
     * @param array $data POST提交接口参数
     * @param bool $isBuildJson
     * @return array
     * @throws InvalidResponseException
     * @throws \WeChat\Exceptions\LocalCacheException
     */
    public function callPostApi($url, array $data, $isBuildJson = true)
    {
        $this->registerApi($url, __FUNCTION__, func_get_args());
        return $this->httpPostForJson($url, $data, $isBuildJson);
    }

    /**
     * 接口通用GET请求方法
     * @param string $url 接口URL
     * @return array
     * @throws InvalidResponseException
     * @throws \WeChat\Exceptions\LocalCacheException
     */
    public function callGetApi($url)
    {
        $this->registerApi($url, __FUNCTION__, func_get_args());
        return $this->httpGetForJson($url);
    }

    /**
     * @param $msg
     * @return DingtalkCrypt|false
     * @throws Exception
     */
    public function checkSignature( &$msg , $debug = false){
        $nonce = input('get.nonce');
        $timeStamp = input('get.timestamp');
        $signature = input('get.signature');
        if(empty($nonce) && empty($timeStamp) && empty($signature)   ) return false;
        $encrypt = json_decode(file_get_contents("php://input"),true);
        $errCode = $this->crypt->DecryptMsg($signature, $timeStamp, $nonce,  $encrypt['encrypt']??'' , $msg);
        if ($debug) p([
            'dateTime' => date('Y-m-d H:i:s'),
            'errCode' => $errCode,
            'request' => $_REQUEST??[],
            'encrypt' => $encrypt,
            'encryptData' => $msg
        ]);
        if($errCode==0 ){
            if($msg->EventType == 'check_url'){
                $this->crypt->EncryptMsg("success",time(),Tools::createNoncestr(),$encryptMsg);
                throw new HttpResponseException(json($encryptMsg,200,['Content-Type:application/json']));
            }
           return $this->crypt;
        }else{
            throw new Exception("消息验证错误：{$errCode}");
        }
    }


    public function decrypt($encrypted, $corpid)
    {
        try {
            $ciphertext_dec = base64_decode($encrypted);
            $iv = substr($this->key, 0, 16);
            $decrypted = openssl_decrypt($ciphertext_dec, 'AES-256-CBC', $this->key, OPENSSL_RAW_DATA|OPENSSL_ZERO_PADDING, $iv);
        } catch (Exception $e) {
            return array(ErrorCode::$DecryptAESError, null);
        }
        try {
            //去除补位字符
            $pkc_encoder = new PKCS7Encoder;
            $result = $pkc_encoder->decode($decrypted);
            //去除16位随机字符串,网络字节序和agent_id
            if (strlen($result) < 16)
                return "";
            $content = substr($result, 16, strlen($result));
            $len_list = unpack("N", substr($content, 0, 4));
            $xml_len = $len_list[1];
            $xml_content = substr($content, 4, $xml_len);
            $from_corpid = substr($content, $xml_len + 4);

        } catch (Exception $e) {
            print $e;
            return array(ErrorCode::$DecryptAESError, null);
        }
        if ($from_corpid != $corpid)
            return array(ErrorCode::$ValidateSuiteKeyError, null);
        return array(0, $xml_content);
    }
}

