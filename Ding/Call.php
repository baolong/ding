<?php

namespace Ding;

use Ding\Contracts\BasicDing;
use Ding\Contracts\Tools;
use think\admin\extend\HttpExtend;

/**
 * 钉钉用户管理
 * Class User
 * @package Ding
 */
class Call extends BasicDing
{

    public function __construct(array $options)
    {
        parent::__construct($options);
        $this->checkSignature();
    }



    /**
     * 注册业务事件回调接口
     * @param array $data
     * @return array
     * @throws Exceptions\InvalidResponseException
     * @throws \WeChat\Exceptions\InvalidResponseException
     * @throws \WeChat\Exceptions\LocalCacheException
     */
    public function register_call_back(array $data){
        $url = "https://oapi.dingtalk.com/call_back/register_call_back?access_token=ACCESS_TOKEN";
        $this->registerApi($url, __FUNCTION__, func_get_args());
        $options['headers'][] = 'Content-Type: application/json; charset=utf-8';
        $data['token'] = $this->config->get('token');
        $data['aes_key'] = $this->config->get('aes_key');
        return Tools::json2arr(HttpExtend::post($url,json_encode($data,JSON_UNESCAPED_UNICODE),$options));
    }


    /**
     * 查询事件回调接口
     * @return array
     * @throws Exceptions\InvalidResponseException
     * @throws \WeChat\Exceptions\InvalidResponseException
     * @throws \WeChat\Exceptions\LocalCacheException
     */
    public function get_call_back(){
        $url = "https://oapi.dingtalk.com/call_back/get_call_back?access_token=ACCESS_TOKEN";
        $this->registerApi($url, __FUNCTION__, func_get_args());
        return Tools::json2arr(HttpExtend::get($url));
    }

    /**
     * 更新事件回调接口
     * @param array $data
     * @return array
     * @throws Exceptions\InvalidResponseException
     * @throws \WeChat\Exceptions\InvalidResponseException
     * @throws \WeChat\Exceptions\LocalCacheException
     */
    public function update_call_back(array $data){
        $url = "https://oapi.dingtalk.com/call_back/update_call_back?access_token=ACCESS_TOKEN";
        $this->registerApi($url, __FUNCTION__, func_get_args());
        $options['headers'][] = 'Content-Type: application/json; charset=utf-8';
        $data['token'] = $this->config->get('token');
        $data['aes_key'] = $this->config->get('aes_key');
        return Tools::json2arr(HttpExtend::post($url,json_encode($data,JSON_UNESCAPED_UNICODE),$options));
    }

    /**
     * 删除事件回调接口
     * @return array
     * @throws Exceptions\InvalidResponseException
     * @throws \WeChat\Exceptions\InvalidResponseException
     * @throws \WeChat\Exceptions\LocalCacheException
     */
    public function delete_call_back(){
        $url = "https://oapi.dingtalk.com/call_back/delete_call_back?access_token=ACCESS_TOKEN";
        $this->registerApi($url, __FUNCTION__, func_get_args());
        return Tools::json2arr(HttpExtend::get($url));
    }

    /**
     * 获取回调失败的结果
     * @return array
     * @throws Exceptions\InvalidResponseException
     * @throws \WeChat\Exceptions\InvalidResponseException
     * @throws \WeChat\Exceptions\LocalCacheException
     */
    public function get_call_back_failed_result(){
        $url = "https://oapi.dingtalk.com/call_back/delete_call_back?access_token=ACCESS_TOKEN";
        $this->registerApi($url, __FUNCTION__, func_get_args());
        return Tools::json2arr(HttpExtend::get($url));
    }

}