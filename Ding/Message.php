<?php

namespace Ding;

use Ding\Contracts\BasicDing;
use Ding\Contracts\Tools;
use think\admin\extend\HttpExtend;

/**
 * 工作通知
 * Class Message
 * @package Ding
 */
class Message extends BasicDing
{

    public function send($data){
        $url = "https://oapi.dingtalk.com/topapi/message/corpconversation/asyncsend_v2?access_token=ACCESS_TOKEN";
        $this->registerApi($url, __FUNCTION__, func_get_args()); 
        return Tools::json2arr(HttpExtend::post($url, Tools::arr2json($data) ,['headers' => ['Content-Type: application/json'] ]));
    }


}