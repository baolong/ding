<?php

namespace Ding;

use Ding\Contracts\BasicDing;
use Ding\Contracts\Tools;
use think\admin\extend\HttpExtend;

/**
 * 钉钉钉盘空间管理
 * Class User
 * @package Ding
 */
class Workflow extends BasicDing
{
    /**
     * 获取角色列表
     * @param int $offset
     * @param int $size
     * @return array
     * @throws Exceptions\InvalidResponseException
     * @throws \WeChat\Exceptions\InvalidResponseException
     * @throws \WeChat\Exceptions\LocalCacheException
     */
    public function query(int $size=100,int $offset=0){
        $url = "https://api.dingtalk.com/v1.0/workflow/processes/userVisibilities/templates";
        $this->registerApi($url, __FUNCTION__, func_get_args());
        $options['headers'][] = "x-acs-dingtalk-access-token:{$this->access_token}";
        return Tools::json2arr(HttpExtend::get($url,['maxResults' => $size,'nextToken' => $offset],$options));
    }

    /**
     * 创建审批单
     * @param $data
     * @return array
     * @throws Exceptions\InvalidResponseException
     * @throws \WeChat\Exceptions\InvalidResponseException
     * @throws \WeChat\Exceptions\LocalCacheException
     */
    public function instances($data){
        $url = "https://api.dingtalk.com/v1.0/workflow/processInstances";
        $this->registerApi($url, __FUNCTION__, func_get_args()); 
        $options['headers'][] = "x-acs-dingtalk-access-token:{$this->access_token}";
        $data = "body=" .  json_encode($data,JSON_UNESCAPED_UNICODE); 
        return Tools::json2arr(HttpExtend::post($url,$data ,$options));
    }

    /**
     * 撤销审批单
     * @param $data
     * @return array
     * @throws Exceptions\InvalidResponseException
     * @throws \WeChat\Exceptions\InvalidResponseException
     * @throws \WeChat\Exceptions\LocalCacheException
     */
    public function terminate($data){
        $url = "https://api.dingtalk.com/v1.0/workflow/processInstances/terminate";
        $this->registerApi($url, __FUNCTION__, func_get_args());
        $options['headers'][] = "x-acs-dingtalk-access-token:{$this->access_token}";
        $data = "body=" .  json_encode($data,JSON_UNESCAPED_UNICODE);
        return Tools::json2arr(HttpExtend::post($url,$data ,$options));
    }

    /**
     * 查看审批单
     * @param $processInstanceId
     * @return array
     * @throws Exceptions\InvalidResponseException
     * @throws \WeChat\Exceptions\InvalidResponseException
     * @throws \WeChat\Exceptions\LocalCacheException
     */
    public function processInstanceId($processInstanceId){
        $url = "https://api.dingtalk.com/v1.0/workflow/processInstances";
        $this->registerApi($url, __FUNCTION__, func_get_args());
        $options['headers'][] = "x-acs-dingtalk-access-token:{$this->access_token}";
        return Tools::json2arr(HttpExtend::get($url,['processInstanceId' => $processInstanceId ] ,$options));
    }

    /**
     * 获取表单 schema
     * @param $processInstanceId
     * @return array
     * @throws Exceptions\InvalidResponseException
     * @throws \WeChat\Exceptions\InvalidResponseException
     * @throws \WeChat\Exceptions\LocalCacheException
     */
    public function getFormsSchemas($processCode){
        $url = "https://api.dingtalk.com/v1.0/workflow/forms/schemas/processCodes";
        $this->registerApi($url, __FUNCTION__, func_get_args());
        $options['headers'][] = "x-acs-dingtalk-access-token:{$this->access_token}";
        return Tools::json2arr(HttpExtend::get($url,['processCode' => $processCode ] ,$options));
    }

}