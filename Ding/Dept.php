<?php

namespace Ding;

use Ding\Contracts\BasicDing;
use Ding\Contracts\Tools;
use think\admin\extend\HttpExtend;

/**
 * 钉钉部门管理
 * Class Dept
 * @package Ding
 */
class Dept extends BasicDing
{

    /**
     * 创建部门
     * @param array $data
     * @return array
     * @throws Exceptions\InvalidResponseException
     * @throws \WeChat\Exceptions\InvalidResponseException
     * @throws \WeChat\Exceptions\LocalCacheException
     */
    public function create(array $data){
        $url = "https://oapi.dingtalk.com/topapi/v2/department/create?access_token=ACCESS_TOKEN";
        $this->registerApi($url, __FUNCTION__, func_get_args());
        return Tools::json2arr(HttpExtend::post($url,$data));
    }

    /**
     * 更新部门
     * @return array
     * @throws Exceptions\InvalidResponseException
     * @throws \WeChat\Exceptions\InvalidResponseException
     * @throws \WeChat\Exceptions\LocalCacheException
     */
    public function update(array $data){
        $url = "https://oapi.dingtalk.com/topapi/v2/department/update?access_token=ACCESS_TOKEN";
        $this->registerApi($url, __FUNCTION__, func_get_args());
        return Tools::json2arr(HttpExtend::post($url,$data));
    }

    /**
     * 删除部门
     * @param string $userid
     * @return array
     * @throws Exceptions\InvalidResponseException
     * @throws \WeChat\Exceptions\InvalidResponseException
     * @throws \WeChat\Exceptions\LocalCacheException
     */
    public function delete(string $id=''){
        $url = "https://oapi.dingtalk.com/topapi/v2/department/delete?access_token=ACCESS_TOKEN";
        $this->registerApi($url, __FUNCTION__, func_get_args());
        return Tools::json2arr(HttpExtend::get($url,['dept_id' => $id]));
    }

    /**
     * 获取用户详情
     * @param string $userid
     * @return array
     * @throws Exceptions\InvalidResponseException
     * @throws \WeChat\Exceptions\InvalidResponseException
     * @throws \WeChat\Exceptions\LocalCacheException
     */
    public function list_ids(string $id=''){
        $url = "https://oapi.dingtalk.com/topapi/v2/department/listsubid?access_token=ACCESS_TOKEN";
        $this->registerApi($url, __FUNCTION__, func_get_args());
        return Tools::json2arr(HttpExtend::get($url,['dept_id' => $id]));
    }

    /**
     * 获取部门列表
     * 本接口只支持获取当前部门的下一级部门基础信息，不支持获取当前部门下所有层级子部门
     * @param array $data
     * @return array
     * @throws Exceptions\InvalidResponseException
     * @throws \WeChat\Exceptions\InvalidResponseException
     * @throws \WeChat\Exceptions\LocalCacheException
     */
    public function list(string $id = ''){
        $url = "https://oapi.dingtalk.com/topapi/v2/department/listsub?access_token=ACCESS_TOKEN";
        $this->registerApi($url, __FUNCTION__, func_get_args());
        return Tools::json2arr(HttpExtend::get($url,['dept_id' => $id,'language' => 'zh_CN']));
    }

    /**
     * 获取部门详情
     * @param string $id
     * @return array
     * @throws Exceptions\InvalidResponseException
     * @throws \WeChat\Exceptions\InvalidResponseException
     * @throws \WeChat\Exceptions\LocalCacheException
     */
    public function get(string $id){
        $url = "https://oapi.dingtalk.com/topapi/v2/department/get?access_token=ACCESS_TOKEN";
        $this->registerApi($url, __FUNCTION__, func_get_args());
        return Tools::json2arr(HttpExtend::get($url,['dept_id' => $id,'language' => 'zh_CN']));
    }

    /**
     * 获取子部门ID列表
     * 本接口只支持获取下一级所有部门ID列表。
     * @param string $id
     * @return array
     * @throws Exceptions\InvalidResponseException
     * @throws \WeChat\Exceptions\InvalidResponseException
     * @throws \WeChat\Exceptions\LocalCacheException
     */
    public function listsubid(string $id = ''){
        $url = "https://oapi.dingtalk.com/topapi/v2/department/listsubid?access_token=ACCESS_TOKEN";
        $this->registerApi($url, __FUNCTION__, func_get_args());
        return Tools::json2arr(HttpExtend::get($url,['dept_id' => $id,'language' => 'zh_CN']));
    }



    /**
     * 获取指定部门的所有父部门列表
     * 调用本接口，获取指定部门的所有父部门ID列表。
     * @return array
     * @throws Exceptions\InvalidResponseException
     * @throws \WeChat\Exceptions\InvalidResponseException
     * @throws \WeChat\Exceptions\LocalCacheException
     */
    public function list_parent_depts(string $id){
        $url = "https://oapi.dingtalk.com/topapi/v2/department/listparentbydept?access_token=ACCESS_TOKEN";
        $this->registerApi($url, __FUNCTION__, func_get_args());
        return Tools::json2arr(HttpExtend::get($url,['dept_id' => $id]));
    }


    /**
     * 获取指定用户的所有父部门列表
     * 调用本接口，查询指定用户所属的所有父级部门。
     * @return array
     * @throws Exceptions\InvalidResponseException
     * @throws \WeChat\Exceptions\InvalidResponseException
     * @throws \WeChat\Exceptions\LocalCacheException
     */
    public function list_parent_users(string $userid = ''){
        $url = "https://oapi.dingtalk.com/topapi/v2/department/listparentbyuser?access_token=ACCESS_TOKEN";
        $this->registerApi($url, __FUNCTION__, func_get_args());
        return Tools::json2arr(HttpExtend::get($url,['userid' => $userid]));
    }



}